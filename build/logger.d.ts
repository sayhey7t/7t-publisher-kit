import 'winston-daily-rotate-file';
export declare const initLogger: (level: string, enable: boolean) => import("winston").Logger;
