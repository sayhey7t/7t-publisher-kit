import { ConnectionsResponse, InitParams, SendEventOptions, UserChannelsResponse } from './types';
export default class PublisherKit {
    private _serverApiKey;
    private _appId;
    private _host;
    private _version;
    private _logger;
    private _initialized;
    init(options: InitParams): this;
    sendEvent(options: SendEventOptions): Promise<void>;
    joinUsersToChannels(userIds: string[], channels: string[]): Promise<void>;
    removeUsersFromChannels(userIds: string[], channels: string[]): Promise<void>;
    deleteUserSessions(userId: string): Promise<void>;
    getUserChannels(userId: string): Promise<UserChannelsResponse>;
    getAllConnections(): Promise<ConnectionsResponse>;
    getChannelConnections(channel: string): Promise<ConnectionsResponse>;
}
