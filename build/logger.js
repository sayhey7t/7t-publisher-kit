"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("winston-daily-rotate-file");
var path_1 = __importDefault(require("path"));
var winston_1 = require("winston");
var appLabel = "publisher-kit";
var combine = winston_1.format.combine, colorize = winston_1.format.colorize, timestamp = winston_1.format.timestamp, label = winston_1.format.label, printf = winston_1.format.printf;
/* TODO : winston-daily-rotate-file
 */
var customFormat = function (_a) {
    var _b = (_a === void 0 ? {} : _a).stringify, stringify = _b === void 0 ? false : _b;
    return printf(function (info) {
        var preMessage = info.timestamp + " " + info.label + " " + info.level + ":";
        var message = info.message;
        if (info.object) {
            if (stringify) {
                return preMessage + " \n" + JSON.stringify(message, null, 2);
            }
            return preMessage + " \n" + message;
        }
        return preMessage + " " + message;
    });
};
var enumerateErrorFormat = winston_1.format(function (info) {
    if (info.message instanceof Error) {
        info.message = __assign({ message: info.message.stack }, info.message);
        info.messageError = true;
    }
    if (info instanceof Error) {
        info.messageError = true;
        return __assign({ message: info.stack }, info);
    }
    if (info.message instanceof Object) {
        info.object = true;
    }
    return info;
});
exports.initLogger = function (level, enable) {
    return winston_1.createLogger({
        level: level,
        silent: !enable,
        format: enumerateErrorFormat(),
        transports: [
            new winston_1.transports.DailyRotateFile({
                filename: 'application-%DATE%.log',
                datePattern: 'YYYY-MM-DD',
                zippedArchive: false,
                maxSize: '20m',
                maxFiles: '15',
                dirname: path_1.default.resolve(__dirname, '..', 'logs', 'publisher-kit'),
                // NOTE: Do not use colorize file transport to avoid special characters
                format: combine(label({ label: appLabel }), timestamp(), customFormat({ stringify: true })),
            }),
            new winston_1.transports.Console({
                format: combine(label({ label: appLabel }), timestamp(), colorize({ all: true }), 
                // NOTE: Do not stringify objects as colorize does it
                customFormat({ stringify: false })),
            }),
        ],
    });
};
