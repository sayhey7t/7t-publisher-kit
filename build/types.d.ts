declare type LogLevel = 'error' | 'warn' | 'help' | 'data' | 'info' | 'debug' | 'prompt' | 'verbose' | 'input' | 'silly';
export interface InitParams {
    host: string;
    serverApiKey: string;
    appId: string;
    version?: string;
    sendUserStatus?: boolean;
    logLevel?: LogLevel;
    logEnabled?: boolean;
}
interface SendEventOptionsBase {
    event: string;
    payload: object;
}
interface SendEventOptionsChannels extends SendEventOptionsBase {
    channels: string[];
}
interface SendEventOptionsUsers extends SendEventOptionsBase {
    users: string[];
}
interface SendEventOptionsBoth extends SendEventOptionsBase {
    channels: string[];
    users: string[];
}
export declare type SendEventOptions = SendEventOptionsChannels | SendEventOptionsUsers | SendEventOptionsBoth;
export interface JoinOrRemoveUserRequestBody {
    channels: string[];
    userIds: string[];
}
export interface ConnectionsResponse {
    count: number;
}
export interface UserChannelsResponse {
    channels: string[];
}
export {};
