import rp from 'request-promise';
import { Logger } from 'winston';

import { initLogger } from './logger';
import {
  ConnectionsResponse,
  InitParams,
  JoinOrRemoveUserRequestBody,
  SendEventOptions,
  UserChannelsResponse,
} from './types';

export default class PublisherKit {
  private _serverApiKey = '';

  private _appId = '';

  private _host = '';

  private _version = 'v1';

  private _logger!: Logger;

  private _initialized() {
    return !!(this._serverApiKey && this._appId);
  }

  public init(options: InitParams) {
    this._serverApiKey = options.serverApiKey;
    this._appId = options.appId;
    this._logger = initLogger(options.logLevel || 'debug', !!options.logEnabled);
    this._host = options.host;
    this._version = options.version || this._version;
    return this;
  }

  public async sendEvent(options: SendEventOptions) {
    try {
      await rp({
        method: 'POST',
        headers: {
          'x-publisher-server-api-key': this._serverApiKey,
        },
        uri: `${this._host}/${this._version}/apps/${this._appId}/events`,
        body: options,
        json: true,
      });
    } catch (err) {
      this._logger.error(options);
      this._logger.error({ err });
      throw new Error(`Failure to send event to supplied channels`);
    }
  }

  public async joinUsersToChannels(userIds: string[], channels: string[]) {
    if (this._initialized()) {
      try {
        await rp({
          method: 'POST',
          headers: {
            'x-publisher-server-api-key': this._serverApiKey,
          },
          uri: `${this._host}/${this._version}/apps/${this._appId}/channels/join`,
          body: {
            channels,
            userIds,
          } as JoinOrRemoveUserRequestBody,
          json: true,
        });
      } catch (err) {
        this._logger.error({ err });
        throw new Error(`Failure to join user ${userIds.join(', ')} to supplied channels`);
      }
    } else {
      throw new Error('Publisher kit needs to be initialized first!');
    }
  }

  public async removeUsersFromChannels(userIds: string[], channels: string[]) {
    if (this._initialized()) {
      try {
        await rp({
          method: 'POST',
          headers: {
            'x-publisher-server-api-key': this._serverApiKey,
          },
          uri: `${this._host}/${this._version}/apps/${this._appId}/channels/leave`,
          body: {
            channels,
            userIds,
          } as JoinOrRemoveUserRequestBody,
          json: true,
          resolveWithFullResponse: true,
        });
      } catch (err) {
        this._logger.error({ err });
        throw new Error(`Failure to remove user ${userIds.join(', ')} from supplied channels`);
      }
    } else {
      throw new Error('Publisher kit needs to be initialized first!');
    }
  }

  public async deleteUserSessions(userId: string) {
    if (this._initialized()) {
      try {
        await rp({
          method: 'DELETE',
          headers: {
            'x-publisher-server-api-key': this._serverApiKey,
          },
          uri: `${this._host}/${this._version}/apps/${this._appId}/users/${userId}`,
          json: true,
        });
      } catch (err) {
        this._logger.error({ err });
        throw new Error(`Failure to delete all user's sessions for user ${userId}.`);
      }
    } else {
      throw new Error('Publisher kit needs to be initialized first!');
    }
  }

  public async getUserChannels(userId: string) {
    if (this._initialized()) {
      try {
        return (await rp({
          method: 'GET',
          headers: {
            'x-publisher-server-api-key': this._serverApiKey,
          },
          uri: `${this._host}/${this._version}/apps/${this._appId}/users/${userId}/channels`,
          json: true,
        })) as UserChannelsResponse;
      } catch (err) {
        this._logger.error({ err });
        throw new Error(`Failure to get user channels for ${userId}.`);
      }
    } else {
      throw new Error('Publisher kit needs to be initialized first!');
    }
  }

  public async getAllConnections() {
    if (this._initialized()) {
      try {
        return (await rp({
          method: 'GET',
          headers: {
            'x-publisher-server-api-key': this._serverApiKey,
          },
          uri: `${this._host}/${this._version}/apps/${this._appId}/connections`,
          json: true,
        })) as ConnectionsResponse;
      } catch (err) {
        this._logger.error({ err });
        throw new Error(`Failure to get connections.`);
      }
    } else {
      throw new Error('Publisher kit needs to be initialized first!');
    }
  }

  public async getChannelConnections(channel: string) {
    if (this._initialized()) {
      try {
        return (await rp({
          method: 'GET',
          headers: {
            'x-publisher-server-api-key': this._serverApiKey,
          },
          uri: `${this._host}/${this._version}/apps/${this._appId}/channels/${channel}/connections`,
          json: true,
        })) as ConnectionsResponse;
      } catch (err) {
        this._logger.error({ err });
        throw new Error(`Failure to get channel connections for ${channel}.`);
      }
    } else {
      throw new Error('Publisher kit needs to be initialized first!');
    }
  }
}
